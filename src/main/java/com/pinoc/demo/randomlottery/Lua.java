package com.pinoc.demo.randomlottery;

/**
 * @author pinoc
 * @date 2023/11/26
 */
public class Lua {
	public static void main(String[] args) {
		// stock: [-1]-库存不限,[0]-没有库存,[>0]-剩余库存
		// return: [-1]-库存不限,[-2]-库存不足, [-3]-库存未初始化 [>=0]-剩余库存
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("if(redis.call('exists',KEYS[1]) == 1) then");
		stringBuilder.append(" local stock = tonumber(redis.call('get',KEYS[1]));");
		stringBuilder.append(" local num = tonumber(ARGV[1]);");
		stringBuilder.append(" if(stock == -1) then");
		stringBuilder.append("   return -1;");
		stringBuilder.append(" end;");
		stringBuilder.append(" if(stock >= num) then");
		stringBuilder.append("   return redis.call('incrby',KEYS[1],0-num);");
		stringBuilder.append(" end;");
		stringBuilder.append(" return -2;");
		stringBuilder.append("end;");
		stringBuilder.append("return -3;");
		String s = stringBuilder.toString();
		System.out.println("lua str = " + s);

	}
}
