package com.pinoc;

import cn.hutool.core.io.FileUtil;
import org.bson.BSONObject;
import org.bson.BasicBSONObject;
import org.bson.BsonDateTime;
import org.bson.types.BSONTimestamp;

import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<String> list1 = Arrays.asList("1","2");
        List<String> list2 = Arrays.asList("4","3");
        //判断是否有交集 没有true 有false
        System.out.println(Collections.disjoint(list1, list2));

        //二进制转换
        long l = 6894520084677525507L;
        String s = Long.toBinaryString(l);
        String substring = s.substring(0,s.length() - 32);
        System.out.println(substring);
        long l1 = Long.parseLong(substring, 2);
        System.out.println(l1);
        substringTest();

    }

    public static void substringTest() {
        String s= "13177805059";
        s = "12" + s.substring(2,s.length());
        System.out.println(s);
    }
}
