package com.pinoc.aop;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

/**
 * 自定义转换器实现向jvm中注册转换器实现aop
 *
 * @author yinpeng10
 */
public class MyTransformer implements ClassFileTransformer {

	@Override
	public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
		System.out.println("类加载器: "+loader.getClass());
		System.out.println("当前类名: " + className);
		//返回null代表不对字节码进行转换处理
		return null;
	}
}
