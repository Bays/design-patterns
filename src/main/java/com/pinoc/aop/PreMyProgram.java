package com.pinoc.aop;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;

/**
 * java探针初探 探针技术测试
 * @author yinpeng10
 */
public class PreMyProgram {
	/**
	 *
	 * @param args -javaagent启动参数的jarpath的值通过此参数传递
	 * @param instrumentation 用于注册ClassFileTransFormer
	 */
	public static void premain(String args, Instrumentation instrumentation) {
		ClassFileTransformer transformer = new MyTransformer();
		//调用addTransformer注册到JVM中
		instrumentation.addTransformer(transformer);
	}

	public static void premain(String args) {
		System.out.println("----premain方法执行2");
		System.out.println(args);
	}
	public static void main(String[] args) {
		System.out.println("hello...");
	}

}
