package com.pinoc.xml;

import cn.hutool.core.collection.SpliteratorUtil;
import cn.hutool.core.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

/**
 * @author pinoc
 * @date 2024/4/18
 */
public class XmlTest {
	public static void main(String[] args) throws TransformerException {
		Document doc=  XmlUtil.readXML("E:\\project\\gitee\\design-patterns\\src\\main\\java\\com\\pinoc\\xml\\message.xml");
		String elePath = "Message.message.message";
		buildElementValue(doc, elePath, "0002507");
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource(doc), new StreamResult(writer));
		String s = writer.toString();
		System.out.println(s);
	}
	private static void updateElementValue(Document doc, String tagName, String value) {
		NodeList nodeList = doc.getElementsByTagName(tagName);
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			node.setTextContent(value);
		}
	}

	private static void updateElementValue(Element element, String tagName, String value) {
		NodeList nodeList = element.getElementsByTagName(tagName);
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			node.setTextContent(value);
		}
	}
	private static void buildElementValue(Document doc, String elementPath, String value) {
		Element retElement = null;
		String[] split = elementPath.split("\\.");
		for (String s : split) {
			NodeList nodeList = doc.getElementsByTagName(s);
			if (nodeList.getLength() == 1) {
				retElement = (Element) nodeList.item(0);
			}
		}

		Node firstChild = retElement.getFirstChild();
		firstChild.setTextContent(value);
//		NodeList nodeList = retElement.getElementsByTagName(tagName);
//		for (int i = 0; i < nodeList.getLength(); i++) {
//			Node node = nodeList.item(i);
//			node.setTextContent(value);
//		}
	}

	private static String getNestedTagValue(Element element, String[] tags, int index) {
		if (index == tags.length - 1) {
			// 达到最后一个标签，返回该标签的值
			NodeList nodeList = element.getElementsByTagName(tags[index]);
			if (nodeList.getLength() == 1) {
				Node node = nodeList.item(0);
				return node.getTextContent();
			}
		} else {
			// 继续递归查找下一级标签
			NodeList nodeList = element.getElementsByTagName(tags[index]);
			if (nodeList.getLength() == 1) {
				Node node = nodeList.item(0);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element nestedElement = (Element) node;
					return getNestedTagValue(nestedElement, tags, index + 1);
				}
			}
		}

		return null;
	}

	private static Node getNestedTagValueV2(Element element, String[] tags, int index) {
		if (index == tags.length - 1) {
			// 达到最后一个标签，返回该标签的值
			NodeList nodeList = element.getElementsByTagName(tags[index]);
			if (nodeList.getLength() == 1) {
				Node node = nodeList.item(0);
				return node;
			}
		} else {
			// 继续递归查找下一级标签
			NodeList nodeList = element.getElementsByTagName(tags[index]);
			if (nodeList.getLength() == 1) {
				Node node = nodeList.item(0);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element nestedElement = (Element) node;
					return getNestedTagValueV2(nestedElement, tags, index + 1);
				}
			}
		}
		return null;
	}
}
