package com.pinoc.xml;
import cn.hutool.core.collection.SpliteratorUtil;
import cn.hutool.core.util.XmlUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;

/**
 * @author pinoc
 * @date 2024/4/18
 */
public class XmlParser2 {
	public static Element getValueByTagPath(Document document, String tagPath) {
		try {
			Element root = document.getDocumentElement();
			String[] tags = tagPath.split("\\.");
			Element currentElement = root;
			for (String tag : tags) {
				NodeList nodeList = currentElement.getElementsByTagName(tag);
				if (nodeList.getLength() > 0) {
					Element element = (Element) nodeList.item(0);
					if (tag.equals(tags[tags.length - 1])) {
						return element;
					} else {
						currentElement = element;
					}
				} else {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void writeElementValue(Document document, String tagPath, String newValue) {
		Element root = document.getDocumentElement();
		String[] tags = tagPath.split("\\.");
		Element currentElement = root;
		for (String tag : tags) {
			NodeList nodeList = currentElement.getElementsByTagName(tag);
			if (nodeList.getLength() > 0) {
				Element element = (Element) nodeList.item(0);
				if (tag.equals(tags[tags.length - 1])) {
					element.setTextContent(newValue);
				} else {
					currentElement = element;
				}
			} else {
				break;
			}
		}

	}

	private static void updateElementValue(Element element, String tagName, String value) {
		NodeList nodeList = element.getElementsByTagName(tagName);
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			node.setTextContent(value);
		}
	}

	public static String doc2Str(Document doc) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource(doc), new StreamResult(writer));
		return writer.toString();
	}

	public static void main(String[] args) throws TransformerException {
		String filePath = "E:\\project\\gitee\\design-patterns\\src\\main\\java\\com\\pinoc\\xml\\message.xml";
		Document document = XmlUtil.readXML(filePath);
		Element element = getValueByTagPath(document, "returnsms.message.content");
		System.out.println("old value: " + element.getTextContent());
		writeElementValue(document, "returnsms.message.content", "哈哈哈哈");
		String s = doc2Str(document);
		System.out.println(s);
	}
}
