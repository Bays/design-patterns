package com.pinoc.Test;

import com.alibaba.fastjson.JSON;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author yinpeng10
 * Hashmap 1.8遍历打的报错
 *
 */
public class test8 {
	private static final int batch = 50;
	public static void main(String[] args) throws UnsupportedEncodingException {
		List<User> userList = new ArrayList<>();

		User u1 = new User("name","address111111");
		userList.add(u1);

		User u2 = new User();
		u2.setName("name2");
		u2.setAddress("dd233333");
		userList.add(u2);

		User u3 = new User();
		u3.setName("name");
		u3.setAddress("address32222");
		userList.add(u3);

		Map<String,String> map = new HashMap<>();
		for (User user : userList) {
			map.put(user.getName(),user.getAddress());
		}
		System.out.println(map);

//		Map<String, String> collect = userList.stream().collect(Collectors.toMap(User::getName, User::getAddress));
//		System.out.println(collect);

		HashMap<Object, Object> solution = userList.stream().collect(HashMap::new, (m, v) -> m.put(v.getName(), v), HashMap::putAll);
		System.out.println(solution);


		String name = "2020学\n" +
				"生2245";
		byte[] bytes = name.getBytes();
		String s = new String(bytes);
		String[] split = s.split("\n");
		String ret = "";
		for (String s1 : split) {
			ret+=s1;
		}
		System.out.println(ret);

		List<Long> customerIds = new ArrayList<>();
		for (int i = 0; i < 678; i++) {
			customerIds.add((long)i);
		}
		for (int i = 0; i < customerIds.size(); i+=batch) {
			int end = Math.min(customerIds.size(), i+batch);
			List<Long> queryIds = customerIds.subList(i, end);
			System.out.println("queryIds size" + queryIds.size());
		}


	}


}
