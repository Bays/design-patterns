package com.pinoc.sentinel;

import com.alibaba.csp.sentinel.annotation.SentinelResource;

/**
 * @author yinpeng10
 */
public class Hello {

	@SentinelResource("hello")
	public void hello(){
		System.out.println("hello world method");
	}
}
