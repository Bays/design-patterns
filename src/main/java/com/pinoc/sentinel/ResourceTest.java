package com.pinoc.sentinel;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;

import java.util.ArrayList;
import java.util.List;

/**
 * sentinel测试小例子
 * @author yinpeng10
 */
public class ResourceTest {
	public static void main(String[] args) {
		initRules();
		int count = 0;
		while (true) {
			try (Entry e = SphU.entry("hello")){
				System.out.println("hello world...");
			}catch(BlockException ex) {
				System.out.println("bloked");
			}
			count++;
		}
	}

	private static void initRules() {
		List<FlowRule> rules= new ArrayList<>();
		FlowRule rule = new FlowRule();
		rule.setResource("hello");
		rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
		rule.setCount(20);
		rules.add(rule);
		FlowRuleManager.loadRules(rules);
	}
}
