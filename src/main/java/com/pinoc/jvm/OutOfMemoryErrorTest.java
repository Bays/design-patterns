package com.pinoc.jvm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yinpeng10
 * @desc
 */
public class OutOfMemoryErrorTest {
	static class OOMObject{}
	public static void main(String[] args) {
		List<OOMObject> list = new ArrayList<>();
		while (true) {
			list.add(new OOMObject());
		}
	}
}
