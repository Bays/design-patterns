package com.pinoc.imagecombiner;

import cn.hutool.core.img.ImgUtil;
import com.freewayso.image.combiner.ImageCombiner;
import com.freewayso.image.combiner.enums.OutputFormat;

import java.awt.image.BufferedImage;
import java.io.InputStream;

/**
 * @author pinoc
 * @date 2024/1/31
 */
public class imageCombiner {
	public static void main(String[] args) throws Exception {
		//合成器（指定背景图和输出格式，整个图片的宽高和相关计算依赖于背景图，所以背景图的大小是个基准）
		BufferedImage bufferedImage = ImgUtil.read("E:\\111111.jpg");
		ImageCombiner combiner = new ImageCombiner(bufferedImage, OutputFormat.JPG);
		//加图片元素
//		combiner.addImageElement("http://xxx.com/image/product.png", 0, 300);
		//加文本元素
		combiner.addTextElement("周末大放送", 80, 100, 1060);
		//执行图片合并
		combiner.combine();
		//可以获取流（并上传oss等）
	//			InputStream is = combiner.getCombinedImageStream();
		//也可以保存到本地
		combiner.save("E:\\document\\zybank\\系统\\IOP\\配置图片\\output_image.jpg");
	}
}
