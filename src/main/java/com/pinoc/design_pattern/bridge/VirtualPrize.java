package com.pinoc.design_pattern.bridge;

/**
 * @author pinoc
 * @date 2023/10/22
 */
public class VirtualPrize extends Prize{
	public VirtualPrize(DrawWay drawWay) {
		super(drawWay);
	}

	@Override
	public void drawPrize() {
		drawWay.drawPrize();
	}
}
