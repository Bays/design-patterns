package com.pinoc.design_pattern.bridge;

/**
 * @author pinoc
 * @date 2023/10/22
 */
public class RealDrawWay implements DrawWay {
	@Override
	public void drawPrize() {
		System.out.println("实物奖品领取方式.....");
	}
}
