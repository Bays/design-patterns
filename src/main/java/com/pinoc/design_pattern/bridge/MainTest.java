package com.pinoc.design_pattern.bridge;

/**
 * @author pinoc
 * @date 2023/10/22
 */
public class MainTest {
	public static void main(String[] args) {
		DrawWay realDrawInfo = new RealDrawWay();
		DrawWay virtualDrawInfo = new VirtualDrawWay();

		Prize realPrize = new RealPrize(realDrawInfo);
		realPrize.drawPrize();

		Prize virtualPrize = new VirtualPrize(virtualDrawInfo);
		virtualPrize.drawPrize();
	}
}
