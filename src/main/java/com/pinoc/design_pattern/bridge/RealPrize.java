package com.pinoc.design_pattern.bridge;

/**
 * @author pinoc
 * @date 2023/10/22
 */
public class RealPrize extends Prize{

	public RealPrize(DrawWay prizeDrawInfo) {
		super(prizeDrawInfo);
	}

	@Override
	public void drawPrize() {
		drawWay.drawPrize();
	}
}
