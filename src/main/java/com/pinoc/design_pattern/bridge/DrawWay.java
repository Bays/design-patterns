package com.pinoc.design_pattern.bridge;

/**
 * @author pinoc
 */
public interface DrawWay {
	void drawPrize();
}
