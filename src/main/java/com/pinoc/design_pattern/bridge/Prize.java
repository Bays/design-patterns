package com.pinoc.design_pattern.bridge;

/**
 * @author pinoc
 * @date 2023/10/22
 * 奖品需要领取，领取前需要完善领取信息
 * 奖品分为、实物、虚拟奖品
 */
public abstract class Prize {
	protected DrawWay drawWay;

	public Prize(DrawWay drawWay) {
		this.drawWay = drawWay;
	}

	public abstract void drawPrize();
}
