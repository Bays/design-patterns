package com.pinoc.design_pattern.p5_2;

import com.pinoc.design_pattern.p5_2.observer.CurrentConditionsDisplay;
import com.pinoc.design_pattern.p5_2.subject.WeatherDarta;

/**
 * @author yinpeng
 */
public class TestWeatherData {

	public static void main(String[] args) {
		WeatherDarta weatherDarta = new WeatherDarta();
		CurrentConditionsDisplay display = new CurrentConditionsDisplay(weatherDarta);

		weatherDarta.setMeasurements(80, 65, 30.4f);


		weatherDarta.setMeasurements(90, 65, 30.4f);

		weatherDarta.setMeasurements(70, 65, 30.4f);
	}
}
