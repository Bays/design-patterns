package com.pinoc.design_pattern.p5_2.observer;

import com.pinoc.design_pattern.p5_2.subject.Subject;

/**
 * @author yinpeng
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement {

	private float temp;
	private float humidity;

	private Subject weatherData;

	public CurrentConditionsDisplay(Subject weatherData) {
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
	}

	@Override
	public void display() {
		System.out.println("current conditions " + temp + " F degrees and " + humidity +" % humidity");
	}

	@Override
	public void update(float temp, float humidity, float pressure) {
		this.temp = temp;
		this.humidity = humidity;
		display();
	}
}
