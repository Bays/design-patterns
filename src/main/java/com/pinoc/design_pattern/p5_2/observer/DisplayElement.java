package com.pinoc.design_pattern.p5_2.observer;

/**
 * @author yinpeng
 */
public interface DisplayElement {
	void display();
}
