package com.pinoc.design_pattern.p5_2.observer;

/**
 * @author yinpeng
 */
public interface Observer {
	void update(float temp, float humidity, float pressure); //推的方式通知
}
