package com.pinoc.design_pattern.p5_2.subject;

import com.pinoc.design_pattern.p5_2.observer.Observer;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author yinpeng
 */
public class WeatherDarta implements Subject {

	private ArrayList<Observer> arrayList = null;

	private float temp;

	private float humidity;

	private float pressure;

	public WeatherDarta() {
		arrayList = new ArrayList<>();
	}

	@Override
	public void registerObserver(Observer o) {
		arrayList.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		int i = arrayList.indexOf(o);
		if(i>=0) {
			arrayList.remove(i);
		}
	}

	@Override
	public void notifyObservers() {
		for (Object o : arrayList) {
			Observer observer = (Observer) o;
			observer.update(temp,humidity,pressure);
		}
	}

	public void  measurementsChanged() {
		notifyObservers();
	}

	public float getTemp() {
		return temp;
	}

	public float getHumidity() {
		return humidity;
	}

	public float getPressure() {
		return pressure;
	}

	public void setMeasurements(float temp, float humidity, float pressure) {
		System.out.println("设置天气数据---"+ new Date());
		this.temp = temp;
		this.humidity = humidity;
		this.pressure = pressure;
		measurementsChanged();
	}
}
