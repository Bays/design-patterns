package com.pinoc.design_pattern.p5_2.subject;

import com.pinoc.design_pattern.p5_2.observer.Observer;

/**
 * @author yinpeng
 */
public interface Subject {

	void registerObserver(Observer o);
	void removeObserver(Observer o);
	void notifyObservers();
}

