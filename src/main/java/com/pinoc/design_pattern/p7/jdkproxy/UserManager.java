package com.pinoc.design_pattern.p7.jdkproxy;

/**
 * @author yinpeng10
 */
interface UserManager {

	boolean addUser(int id, String name);

	boolean deleteUser(int id);
}
