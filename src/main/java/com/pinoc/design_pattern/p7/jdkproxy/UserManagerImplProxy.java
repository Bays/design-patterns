package com.pinoc.design_pattern.p7.jdkproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author yinpeng10
 */
public class UserManagerImplProxy implements InvocationHandler {

	private Object proxyTarget;

	/**
	 *
	 * @param proxy 被代理对象
	 * @param method 被代理方法
	 * @param args 被代理方法的参数
	 * @return
	 * @throws Throwable
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("start proxy..");
		Object invoke = method.invoke(proxyTarget, args);
		System.out.println("end proxy..");
		return invoke;
	}

	public UserManagerImplProxy(Object proxyTarget) {
		this.proxyTarget = proxyTarget;
	}

	public Object getJdkProxy() {
		return Proxy.newProxyInstance(this.proxyTarget.getClass().getClassLoader(),this.proxyTarget.getClass().getInterfaces(), this);
	}
}
