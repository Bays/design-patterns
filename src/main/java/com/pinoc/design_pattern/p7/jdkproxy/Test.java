package com.pinoc.design_pattern.p7.jdkproxy;

/**
 * @author yinpeng10
 */
public class Test {

	public static void main(String[] args) {

		UserManagerImplProxy proxy = new UserManagerImplProxy(new UserManagerImpl());
		UserManager userManager = (UserManager) proxy.getJdkProxy();
		userManager.addUser(1,"aa");
		userManager.addUser(2,"aa");
		userManager.deleteUser(3);
	}
}
