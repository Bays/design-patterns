package com.pinoc.design_pattern.p7.jdkproxy;

/**
 * @author yinpeng10
 */
public class UserManagerImpl implements UserManager{
	@Override
	public boolean addUser(int id,String name) {
		System.out.println("add user...");
		return true;
	}

	@Override
	public boolean deleteUser(int id) {
		System.out.println("del user...");
		return false;
	}
}
