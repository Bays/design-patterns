package com.pinoc.design_pattern.p7.cglib;

import net.sf.cglib.core.DebuggingClassWriter;

import java.util.Properties;

/**
 * @author yinpeng10
 */
public class TestCgLib {

	public static void main(String[] args) {
		Properties prop = new Properties();
		prop.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "E:\\Local Projects\\tem_class");
		System.setProperties(prop);
		CgLibProxy cgLibProxy = new CgLibProxy();
		Hello proxy = (Hello) cgLibProxy.getProxy(Hello.class);
		proxy.say("tom");
	}
}
