package com.pinoc.design_pattern.p7.cglib;

/**
 * @author yinpeng10
 */
public class Hello {
	public void say(String name) {
		System.out.println("hello " + name);
	}
}
