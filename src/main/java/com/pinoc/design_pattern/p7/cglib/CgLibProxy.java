package com.pinoc.design_pattern.p7.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author yinpeng10
 */
public class CgLibProxy implements MethodInterceptor {

	@Override
	public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
		System.out.println("start cglib proxy....");
		Object result = methodProxy.invokeSuper(o, objects);
		return result;
	}

	public Object getProxy(Class cls) {
		Enhancer en = new Enhancer();
		en.setSuperclass(cls);
		en.setCallback(this);
		return en.create();
	}
}
