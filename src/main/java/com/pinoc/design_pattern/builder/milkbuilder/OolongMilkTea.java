package com.pinoc.design_pattern.builder.milkbuilder;

/**
 * @author pinoc
 * @date 2023/12/20
 */
public class OolongMilkTea  extends MilkTea{
	public OolongMilkTea() {
		this.prize = 10.0;
	}
}


