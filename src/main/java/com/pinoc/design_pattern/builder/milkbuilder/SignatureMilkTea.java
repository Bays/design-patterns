package com.pinoc.design_pattern.builder.milkbuilder;

/**
 * @author pinoc
 * @date 2023/12/20
 */
public class SignatureMilkTea extends MilkTea{

	public SignatureMilkTea() {
		this.prize = 5.7;
	}
}
