package com.pinoc.design_pattern.builder.milkbuilder;

/**
 * @author pinoc
 * @date 2023/12/20
 */
public class MilkTea {
	protected double prize;
	protected String topping = "boba";
	protected String tea = "regularMilktea";
	protected int suger = 100;
	public MilkTea() {
		this.prize = 7.0;
	}

	public double getPrize() {
		return prize;
	}

	public void setPrize(double prize) {
		this.prize = prize;
	}
}
