package com.pinoc.design_pattern.builder.milkbuilder;

import sun.font.BidiUtils;

/**
 * @author pinoc
 * @date 2023/12/20
 */
public class MainTest {
	public static void main(String[] args) {
		MilkTeaDirector director = new MilkTeaDirector(new SignatureMilkTeaBuilder());
		director.makeTea();
		director.changeBuilder(new OolongMilkTeaBuilder());
		director.makeTea();

		director.make("sig");
		director.make("oolong");


		CustomMilkTeaBuilder builder = new CustomMilkTeaBuilder();

		builder.reset();
		builder.addTopping("bbbbb");
		builder.addTea("oolonggg");
		builder.addRegularTea(10);
		builder.getProduct();

	}
}
