package com.pinoc.design_pattern.builder.milkbuilder;

/**
 * @author pinoc
 * @date 2023/12/20
 */
public class MilkTeaDirector {
	private MilkTeaBuilder milkTeaBuilder;

	public MilkTeaDirector(MilkTeaBuilder milkTeaBuilder) {
		this.milkTeaBuilder = milkTeaBuilder;
	}

	public void changeBuilder(MilkTeaBuilder milkTeaBuilder) {
		this.milkTeaBuilder = milkTeaBuilder;
	}

	public MilkTea makeTea() {
		this.milkTeaBuilder.reset();
		this.milkTeaBuilder.addTopping();
		this.milkTeaBuilder.addTea();
		this.milkTeaBuilder.addRegularTea();
		return this.milkTeaBuilder.getProduct();
	}

	public MilkTea make(String type) {
		if (type.equals("sig")) {
			this.changeBuilder(new SignatureMilkTeaBuilder());
		} else if(type.equals("oolong")) {
			this.changeBuilder(new OolongMilkTeaBuilder());
		}
		return this.makeTea();
	}
}
