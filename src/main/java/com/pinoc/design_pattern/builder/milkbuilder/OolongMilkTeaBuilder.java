package com.pinoc.design_pattern.builder.milkbuilder;

/**
 * @author pinoc
 * @date 2023/12/20
 */
public class OolongMilkTeaBuilder implements MilkTeaBuilder{
	private OolongMilkTea product;
	@Override
	public void reset() {
		this.product = new OolongMilkTea();
	}

	@Override
	public void addTopping() {
		product.topping = "oolong jelly";
	}

	@Override
	public void addTea() {
		product.tea = "oolong tea";
	}

	@Override
	public void addRegularTea() {
		product.suger=50;
	}

	@Override
	public MilkTea getProduct() {
		System.out.format("oolong milktea %s %s %s \n", this.product.topping, this.product.tea, this.product.suger);
		return this.product;
	}
}
