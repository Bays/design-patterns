package com.pinoc.design_pattern.builder.milkbuilder;

/**
 * @author pinoc
 * @date 2023/12/20
 */
public class SignatureMilkTeaBuilder  implements MilkTeaBuilder{
	private SignatureMilkTea product;
	@Override
	public void reset() {
		this.product = new SignatureMilkTea();
	}

	@Override
	public void addTopping() {
		product.topping = "boba";
	}

	@Override
	public void addTea() {
		product.tea = "signature tea";
	}

	@Override
	public void addRegularTea() {
		product.suger=100;
	}

	@Override
	public MilkTea getProduct() {
		System.out.format("signature milktea %s %s %s \n", this.product.topping, this.product.tea, this.product.suger);
		return this.product;
	}
}
