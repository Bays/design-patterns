package com.pinoc.design_pattern.builder.milkbuilder;

/**
 * @author pinoc
 * @date 2023/12/20
 */
public interface MilkTeaBuilder {
	void reset();

	void addTopping();

	void addTea();

	void addRegularTea();

	MilkTea getProduct();
}
