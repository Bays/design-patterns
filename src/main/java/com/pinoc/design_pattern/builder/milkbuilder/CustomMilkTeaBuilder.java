package com.pinoc.design_pattern.builder.milkbuilder;

/**
 * @author pinoc
 * @date 2023/12/20
 */
public class CustomMilkTeaBuilder{
	private MilkTea product;
	public void reset() {
		this.product = new MilkTea();
	}

	public void addTopping(String topping) {
		product.topping = topping;
	}

	public void addTea(String tea) {
		product.tea = tea;
	}

	public void addRegularTea(Integer suger) {
		product.suger = suger;
	}

	public MilkTea getProduct() {
		System.out.format("custom milktea %s %s %s \n", this.product.topping, this.product.tea, this.product.suger);
		return this.product;
	}
}
