package com.pinoc.design_pattern.builder.demo2;

/**
 * @author pinoc
 * @date 2023/10/22
 */
public class Director {
	private Builder builder;

	public Director(Builder builder) {
		this.builder = builder;
	}

	public void construct() {
		builder.buildPart1();
		builder.buildPart2();
	}
}
