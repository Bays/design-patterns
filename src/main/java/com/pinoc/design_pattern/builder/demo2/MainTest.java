package com.pinoc.design_pattern.builder.demo2;

/**
 * @author pinoc
 * @date 2023/10/22
 */
public class MainTest {
	public static void main(String[] args) {
		Builder builder = new ConcreteBuilder();

		Director director = new Director(builder);

		director.construct();

		Product product = builder.getProduct();
		product.show();
	}
}
