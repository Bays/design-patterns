package com.pinoc.design_pattern.builder.demo2;

/**
 * @author pinoc
 * @date 2023/10/22
 */
public class Product {
	private String part1;

	private String part2;

	public String getPart1() {
		return part1;
	}

	public void setPart1(String part1) {
		this.part1 = part1;
	}

	public String getPart2() {
		return part2;
	}

	public void setPart2(String part2) {
		this.part2 = part2;
	}

	public void show() {
		System.out.println("Part 1: " + part1);
		System.out.println("Part 2: " + part2);
	}
}
