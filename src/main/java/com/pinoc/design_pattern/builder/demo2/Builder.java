package com.pinoc.design_pattern.builder.demo2;

/**
 * @author pinoc
 * @date 2023/10/22
 */
public abstract class Builder {

	protected Product product = new Product();

	public abstract void buildPart1();

	public abstract void buildPart2();

	public Product getProduct() {
		return product;
	}
}
