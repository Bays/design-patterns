package com.pinoc.design_pattern.builder.demo2;

/**
 * @author pinoc
 * @date 2023/10/22
 */
public class ConcreteBuilder extends Builder{
	@Override
	public void buildPart1() {
		product.setPart1("part1");
	}

	@Override
	public void buildPart2() {
		product.setPart2("part2");
	}
}
