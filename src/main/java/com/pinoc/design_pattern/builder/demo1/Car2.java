package com.pinoc.design_pattern.builder.demo1;

/**
 *
 * @author yinpeng10
 */
public class Car2 {

	private  String window; //可选

	private  String tire; //可选

	private  String frame; //必选

	public Car2(String frame) {
		this.frame = frame;
	}

	public Car2() {
	}

	public String getWindow() {
		return window;
	}

	public void setWindow(String window) {
		this.window = window;
	}

	public String getTire() {
		return tire;
	}

	public void setTire(String tire) {
		this.tire = tire;
	}

	public String getFrame() {
		return frame;
	}

	public void setFrame(String frame) {
		this.frame = frame;
	}

	@Override
	public String toString() {
		return "Car2{" +
				"window='" + window + '\'' +
				", tire='" + tire + '\'' +
				", frame='" + frame + '\'' +
				'}';
	}
}
