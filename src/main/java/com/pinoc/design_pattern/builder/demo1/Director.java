package com.pinoc.design_pattern.builder.demo1;

/**
 * @author yinpeng10
 */
public class Director {

	public void makeCar2(Builder builder) {
		builder.buildTire();
		builder.buildWindow();
	}
}
