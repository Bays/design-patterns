package com.pinoc.design_pattern.builder.demo1;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author yinpeng10
 */
public class Test {
	public static void main(String[] args) {
//		//简化版建造者模式
		Car build = new Car.Builder("框架").setTire("tire").setWindow("window").build();


		AtomicInteger atomicInteger = new AtomicInteger(0);

		int andAdd = atomicInteger.getAndAdd(10);
		System.out.println(andAdd);
	}
}
