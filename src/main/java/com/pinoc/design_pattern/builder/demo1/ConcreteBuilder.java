package com.pinoc.design_pattern.builder.demo1;

/**
 * 具体建造者
 * @author yinpeng10
 */
public class ConcreteBuilder implements Builder{

	private Car2 car2;

	public ConcreteBuilder(String frame) {
		car2 = new Car2(frame);
	}

	@Override
	public void buildTire() {
		car2.setTire("tire");
	}

	@Override
	public void buildWindow() {
		car2.setWindow("window");
	}

	@Override
	public void buildFrame() {
		car2.setFrame("frame");
	}

	@Override
	public Car2 getCar2() {
		return car2;
	}
}
