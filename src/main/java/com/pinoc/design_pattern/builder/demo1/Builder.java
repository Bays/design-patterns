package com.pinoc.design_pattern.builder.demo1;

/**
 * @author yinpeng10
 */
public interface Builder {

	void buildTire();

	void buildWindow();

	void buildFrame();

	Car2 getCar2();
}
