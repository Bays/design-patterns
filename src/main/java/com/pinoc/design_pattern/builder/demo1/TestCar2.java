package com.pinoc.design_pattern.builder.demo1;

/**
 * @author yinpeng10
 */
public class TestCar2 {

	public static void main(String[] args) {
		Director director = new Director();
		Builder builder = new ConcreteBuilder("frame");
		director.makeCar2(builder);
		Car2 car2 = builder.getCar2();
		System.out.println(car2.toString());
	}
}
