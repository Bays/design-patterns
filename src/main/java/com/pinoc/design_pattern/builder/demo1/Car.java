package com.pinoc.design_pattern.builder.demo1;

/**
 * @author yinpeng10
 */
public class Car {

	private final String window; //可选

	private final String tire; //可选

	private final String frame; //必选

	private Car(Builder builder) {
		this.frame = builder.frame;
		this.tire = builder.tire;
		this.window = builder.window;
	}

	public static class Builder{
		private  String window; //可选

		private  String tire; //可选

		private  String frame; //必选

		public Builder(String frame) {
			this.frame = frame;
		}

		public Builder setTire(String tire) {
			this.tire = tire;
			return this;
		}

		public Builder setWindow(String window) {
			this.window = window;
			return this;
		}

		public Car build() {
			return new Car(this);
		}
	}

	public String getWindow() {
		return window;
	}

	public String getTire() {
		return tire;
	}

	public String getFrame() {
		return frame;
	}
}
