package com.pinoc.design_pattern.p8;

/**
 * 模板方法类
 * @author yinpeng
 */
public abstract class BaseDrink {

	/**
	 * final不可继承 模板方法定义了一些公共的算法
	 */
	final void prepareDrink() {
		boilWater();
		brew();
		pourInCup();
		addCondiments();
	}

	/**
	 * 不同类型子类实现
	 */
	abstract void brew();

	/**
	 * 不同类型子类实现
	 */
	abstract void addCondiments();

	/**
	 * 公共方法
	 */
	void boilWater() {
		System.out.println("boiling water");
	}

	/**
	 * 公共方法
	 */
	void pourInCup() {
		System.out.println("pouring into cup");
	}
}
