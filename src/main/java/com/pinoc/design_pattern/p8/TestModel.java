package com.pinoc.design_pattern.p8;

/**
 * @author yinpeng
 */
public class TestModel {

	public static void main(String[] args) {
		Tea myTea = new Tea();
		myTea.prepareDrink();
		System.out.println("\n");
		Coffee coffee = new Coffee();
		coffee.prepareDrink();
	}
}
