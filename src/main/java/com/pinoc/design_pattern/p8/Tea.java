package com.pinoc.design_pattern.p8;

/**
 * 模板方法测试
 * @author yinpeng
 */
public class Tea extends BaseDrink {

	@Override
	void brew() {
		System.out.println("steeping the tea");
	}

	@Override
	void addCondiments() {
		System.out.println("adding lemon");
	}
}
