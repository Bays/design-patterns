package com.pinoc.design_pattern.p8;

/**
 * @author yinpeng
 */
public class Coffee extends BaseDrink {

	@Override
	void brew() {
		// 滴咖啡
		System.out.println("dripping coffee through filter");
	}

	@Override
	void addCondiments() {
		System.out.println("adding sugar and milk");
	}
}
