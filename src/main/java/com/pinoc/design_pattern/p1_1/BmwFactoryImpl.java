package com.pinoc.design_pattern.p1_1;

/**
 * @author yinpeng10
 */
public class BmwFactoryImpl implements Factory {
	@Override
	public Car driveCar() {
		return new Bmw();
	}
}
