package com.pinoc.design_pattern.p1_1;

/**
 * @author yinpeng10
 */
public class BenzFactoryImpl implements Factory {
	@Override
	public Car driveCar() {
		return new Benz();
	}
}
