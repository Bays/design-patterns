package com.pinoc.design_pattern.p1_1;

/**
 * 抽象产品
 * @author yinpeng10
 */
public interface Car {

	/**
	 * 开车
	 */
	void drive();
}
