package com.pinoc.design_pattern.p1_1;

/**
 * 工厂类角色
 * @author yinpeng10
 */
public interface Factory {

	public Car driveCar();
}
