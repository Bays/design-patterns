package com.pinoc.design_pattern.p1_1;

/**
 * 简单工厂模式 Test
 *
 * @author yinpeng10
 */
public class SimpleFactoryMode {

	public static void main(String[] args) {
		Factory benzFactory = new BenzFactoryImpl();
		Car benzCar = benzFactory.driveCar();
		benzCar.drive();

		Factory bmwFactory = new BmwFactoryImpl();
		Car bmwCar = bmwFactory.driveCar();
		bmwCar.drive();
	}
}
