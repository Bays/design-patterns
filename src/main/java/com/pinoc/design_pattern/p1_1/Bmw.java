package com.pinoc.design_pattern.p1_1;

/**
 * 具体产品
 * @author yinpeng10
 */
public class Bmw implements Car {
	@Override
	public void drive() {
		System.out.println("car bmw");
	}
}
