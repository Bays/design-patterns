package com.pinoc.design_pattern.p1_2;

/**
 * @author yinpeng10
 */
public interface  BusinessCar {
	void produceBusinessCar();
}
