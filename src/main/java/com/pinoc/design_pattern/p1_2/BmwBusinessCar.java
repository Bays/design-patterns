package com.pinoc.design_pattern.p1_2;

/**
 * @author yinpeng10
 */
public class BmwBusinessCar implements BusinessCar {
	@Override
	public void produceBusinessCar() {
		System.out.println("宝马产商务车");
	}
}
