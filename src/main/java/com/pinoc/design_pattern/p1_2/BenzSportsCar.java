package com.pinoc.design_pattern.p1_2;

/**
 * @author yinpeng10
 */
public class BenzSportsCar implements SportsCar {
	@Override
	public void produceSportsCar() {
		System.out.println("奔驰产跑车");
	}
}
