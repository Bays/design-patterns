package com.pinoc.design_pattern.p1_2;

/**
 * @author yinpeng10
 */
public interface Factory {

	/**
	 * 生产跑车
	 */
	SportsCar produceSportsCar();

	/**
	 * 生产商务车
	 */
	BusinessCar produceBusinessCar();
}
