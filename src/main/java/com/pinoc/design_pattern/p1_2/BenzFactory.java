package com.pinoc.design_pattern.p1_2;

/**
 * @author yinpeng10
 */
public class BenzFactory implements Factory {
	@Override
	public SportsCar produceSportsCar() {
		return new BenzSportsCar();
	}

	@Override
	public BusinessCar produceBusinessCar() {
		return new BenzBusinessCar();
	}
}
