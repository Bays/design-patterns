package com.pinoc.design_pattern.p1_2;

/**
 * @author yinpeng10
 */
public class BmwSportsCar implements SportsCar {
	@Override
	public void produceSportsCar() {
		System.out.println("宝马产跑车");
	}
}
