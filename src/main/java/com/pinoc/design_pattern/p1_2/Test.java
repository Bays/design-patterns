package com.pinoc.design_pattern.p1_2;

/**
 * @author yinpeng10
 */
public class Test {
	public static void main(String[] args) {
		Factory benzFactory = new BenzFactory();
		SportsCar sportsCar = benzFactory.produceSportsCar();
		BusinessCar businessCar = benzFactory.produceBusinessCar();
		sportsCar.produceSportsCar();
		businessCar.produceBusinessCar();

		Factory bmwFactory = new BmwFactory();
		BusinessCar businessCar1 = bmwFactory.produceBusinessCar();
		SportsCar sportsCar1 = bmwFactory.produceSportsCar();
		businessCar1.produceBusinessCar();
		sportsCar1.produceSportsCar();
	}
}
