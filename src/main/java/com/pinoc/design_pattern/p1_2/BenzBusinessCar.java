package com.pinoc.design_pattern.p1_2;

/**
 * @author yinpeng10
 */
public class BenzBusinessCar implements BusinessCar {
	@Override
	public void produceBusinessCar() {
		System.out.println("奔驰产商务车");
	}
}
