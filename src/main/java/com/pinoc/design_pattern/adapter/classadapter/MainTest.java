package com.pinoc.design_pattern.adapter.classadapter;

/**
 * @author pinoc
 * @date 2023/12/13
 */
public class MainTest {
	public static void main(String[] args) {
		FahrenheitTemperature fahrenheitTemperature = new FahrenheitTemperature(100);
		CelsiusTemperature c = new FahrenheitTemperatureAdapter(fahrenheitTemperature.getTemperature());
		System.out.println("华氏度转成摄氏度之后 " + c.getCTemperature());
	}
}
