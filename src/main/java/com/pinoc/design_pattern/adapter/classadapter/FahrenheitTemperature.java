package com.pinoc.design_pattern.adapter.classadapter;

/**
 * @author pinoc
 * @date 2023/12/13
 */
public class FahrenheitTemperature {

	private double temperature;

	public FahrenheitTemperature(double temperature) {
		this.temperature = temperature;
	}

	public double getTemperature() {
		return temperature;
	}
}
