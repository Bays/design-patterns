package com.pinoc.design_pattern.adapter.classadapter;

/**
 * @author pinoc
 * @date 2023/12/13
 * 类适配器
 */
public class FahrenheitTemperatureAdapter extends FahrenheitTemperature implements CelsiusTemperature{
	public FahrenheitTemperatureAdapter(double temperature) {
		super(temperature);
	}

	@Override
	public double getCTemperature() {
		return (super.getTemperature() - 32) * 5 / 9;
	}
}
