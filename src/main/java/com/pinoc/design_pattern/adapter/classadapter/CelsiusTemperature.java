package com.pinoc.design_pattern.adapter.classadapter;

/**
 * @author pinoc
 * 摄氏度
 */
public interface CelsiusTemperature {
	double getCTemperature();
}
