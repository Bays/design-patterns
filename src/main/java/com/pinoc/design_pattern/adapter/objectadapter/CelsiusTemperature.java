package com.pinoc.design_pattern.adapter.objectadapter;

/**
 * @author pinoc
 * 摄氏度
 */
public interface CelsiusTemperature {
	double getCTemperature();
}
