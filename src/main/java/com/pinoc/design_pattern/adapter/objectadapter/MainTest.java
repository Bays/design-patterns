package com.pinoc.design_pattern.adapter.objectadapter;

/**
 * @author pinoc
 * @date 2023/12/13
 */
public class MainTest {
	public static void main(String[] args) {
		FahrenheitTemperature f = new FahrenheitTemperature(100);
		FahrenheitTemperatureAdapter adapter = new FahrenheitTemperatureAdapter(f);
		System.out.println("华氏度转成摄氏度之后 " + adapter.getCTemperature());
	}
}
