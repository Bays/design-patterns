package com.pinoc.design_pattern.adapter.objectadapter;

/**
 * @author pinoc
 * @date 2023/12/13
 * 对象适配器
 */
public class FahrenheitTemperatureAdapter implements CelsiusTemperature {

	private FahrenheitTemperature fahrenheitTemperature;

	public FahrenheitTemperatureAdapter(FahrenheitTemperature fahrenheitTemperature) {
		this.fahrenheitTemperature = fahrenheitTemperature;
	}

	@Override
	public double getCTemperature() {
		return (fahrenheitTemperature.getTemperature() - 32) * 5 / 9;
	}
}
