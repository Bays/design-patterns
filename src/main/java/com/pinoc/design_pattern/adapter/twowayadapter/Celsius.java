package com.pinoc.design_pattern.adapter.twowayadapter;

/**
 * @author pinoc
 * @date 2023/12/14
 */
public class Celsius implements CelsiusTemperature{
	private double temperature;

	public Celsius(double temperature) {
		this.temperature = temperature;
	}

	@Override
	public double getCTemperature() {
		return this.temperature;
	}
}
