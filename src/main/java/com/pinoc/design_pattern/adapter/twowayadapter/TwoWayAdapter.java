package com.pinoc.design_pattern.adapter.twowayadapter;

/**
 * @author pinoc
 * @date 2023/12/14
 */
public class TwoWayAdapter implements CelsiusTemperature, FahrenheitTemperature{
	private Celsius celsius;
	private Fahrenheit fahrenheit;

	public TwoWayAdapter(Celsius celsius) {
		this.celsius = celsius;
		this.fahrenheit = new Fahrenheit(celsius.getCTemperature() * 9/5 + 32);
	}

	public TwoWayAdapter(Fahrenheit fahrenheit) {
		this.fahrenheit = fahrenheit;
		this.celsius = new Celsius((fahrenheit.getFTemperature() - 32) * 5 / 9);
	}

	@Override
	public double getCTemperature() {
		return celsius.getCTemperature();
	}

	@Override
	public double getFTemperature() {
		return fahrenheit.getFTemperature();
	}
}
