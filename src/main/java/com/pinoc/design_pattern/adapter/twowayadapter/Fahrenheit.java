package com.pinoc.design_pattern.adapter.twowayadapter;

/**
 * @author pinoc
 * @date 2023/12/13
 */
public class Fahrenheit implements FahrenheitTemperature{

	private double temperature;

	public Fahrenheit(double temperature) {
		this.temperature = temperature;
	}

	@Override
	public double getFTemperature() {
		return this.temperature;
	}
}
