package com.pinoc.design_pattern.adapter.twowayadapter;

/**
 * @author pinoc
 * @date 2023/12/14
 */
public class MainTest {
	public static void main(String[] args) {
		Celsius celsius = new Celsius(25);
		TwoWayAdapter twoWayAdapter = new TwoWayAdapter(celsius);
		double fTemperature = twoWayAdapter.getFTemperature();
		System.out.println("25 摄氏度== 华氏度" + fTemperature);

		Fahrenheit fahrenheit = new Fahrenheit(77);
		TwoWayAdapter twoWayAdapter2 = new TwoWayAdapter(fahrenheit);
		System.out.println("77 华氏度 == 摄氏度" + twoWayAdapter2.getCTemperature());
	}
}
