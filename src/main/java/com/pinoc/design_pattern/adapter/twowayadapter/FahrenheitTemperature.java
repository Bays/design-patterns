package com.pinoc.design_pattern.adapter.twowayadapter;

/**
 * @author pinoc
 * 摄氏度
 */
public interface FahrenheitTemperature {
	double getFTemperature();
}
