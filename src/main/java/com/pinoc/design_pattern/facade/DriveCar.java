package com.pinoc.design_pattern.facade;

/**
 * @author pinoc
 * @date 2023/12/17
 */
public class DriveCar {

	private String key;
	private boolean canDrive;

	public DriveCar(String key) {
		this.key = key;
	}

	public void inCar() {
		if("key".equals(key)) {
			this.canDrive = true;
			System.out.println("use key in car...");
		} else {
			System.out.println("the key is useless");
		}
	}

	public void adjustSeat() {
		if(canDrive) {
			System.out.println("can adjust seat");
		}
	}

	public void startCar() {
		if(canDrive) {
			System.out.println("car can starting");
		}
	}
}
