package com.pinoc.design_pattern.facade;

/**
 * @author pinoc
 * @date 2023/12/17
 * 外观简要理解就是对一些组合方法进行封装，可以忽略细节。
 */
public class CarFacade {

	private DriveCar car;

	public CarFacade(DriveCar car) {
		this.car = car;
	}

	public void driveCar() {
		car.inCar();
		car.adjustSeat();
		car.startCar();
	}
}
