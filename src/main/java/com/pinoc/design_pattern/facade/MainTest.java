package com.pinoc.design_pattern.facade;

/**
 * @author pinoc
 * @date 2023/12/17
 */
public class MainTest {
	public static void main(String[] args) {
		CarFacade carFacade = new CarFacade(new DriveCar("key"));
		carFacade.driveCar();
	}
}
