package com.pinoc.design_pattern.templatetest;

/**
 * @author pinoc
 */
public interface BehaviorInterface {
	public void checkParams(Context ctx) throws Exception;

	public void getPrizesByNode(Context ctx) throws Exception;

	public static String runFuncName() {
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		if (stackTrace.length >= 3) {
			return stackTrace[2].getMethodName();
		} else {
			return "";
		}
	}
}