package com.pinoc.design_pattern.templatetest;

/**
 * @author pinoc
 * @date 2023/5/4
 */
public final class Constants {
	public static final int ACT_TYPE_TIME = 1;
	public static final int ACT_TYPE_TIMES = 2;
	public static final int ACT_TYPE_AMOUNT = 3;

	private Constants() {}
}