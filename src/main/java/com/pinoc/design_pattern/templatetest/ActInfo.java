package com.pinoc.design_pattern.templatetest;

/**
 * @author pinoc
 * @date 2023/5/4
 */
public class ActInfo {
	private int activityType;
	// Other fields are omitted

	public ActInfo(int activityType) {
		this.activityType = activityType;
	}

	public int getActivityType() {
		return activityType;
	}

	public void setActivityType(int activityType) {
		this.activityType = activityType;
	}
}