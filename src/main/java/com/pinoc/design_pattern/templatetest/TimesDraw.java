package com.pinoc.design_pattern.templatetest;

/**
 * @author pinoc
 * @date 2023/5/4
 */
public class TimesDraw implements BehaviorInterface {
	@Override
	public void checkParams(Context ctx) throws Exception {
		System.out.println(BehaviorInterface.runFuncName() + "按抽奖次数抽奖类型：特殊参数校验...");
	}

	@Override
	public void getPrizesByNode(Context ctx) throws Exception {
		System.out.println(BehaviorInterface.runFuncName()+ "1. 判断是该用户第几次抽奖...");
		System.out.println(BehaviorInterface.runFuncName() + "2. 获取对应 node 的奖品信息...");
		System.out.println(BehaviorInterface.runFuncName() + "3. 复写原所有奖品信息（抽取该 node 节点的奖品）...");
	}
}
