package com.pinoc.design_pattern.templatetest;

/**
 * @author pinoc
 * @date 2023/5/4
 */
public class TimeDraw implements BehaviorInterface {
	@Override
	public void checkParams(Context ctx) throws Exception {
		System.out.println(BehaviorInterface.runFuncName() + "按时间抽奖类型：特殊参数校验...");
	}

	@Override
	public void getPrizesByNode(Context ctx) throws Exception {
		System.out.println(BehaviorInterface.runFuncName() + "do nothing（抽取该场次的奖品即可，无需其他逻辑）...");
	}


}