package com.pinoc.design_pattern.templatetest;

/**
 * @author pinoc
 * @date 2023/5/4
 */
public class Context {
	private ActInfo actInfo;

	public Context(ActInfo actInfo) {
		this.actInfo = actInfo;
	}

	public ActInfo getActInfo() {
		return actInfo;
	}

	public void setActInfo(ActInfo actInfo) {
		this.actInfo = actInfo;
	}
}