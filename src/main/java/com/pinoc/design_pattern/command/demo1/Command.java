package com.pinoc.design_pattern.command.demo1;

/**
 * @author pinoc
 * @date 2023/10/22
 */
public interface Command {
	void execute();
}
