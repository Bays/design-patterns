package com.pinoc.design_pattern.command.demo1;

/**
 * @author pinoc
 * @date 2023/10/22
 */
public class MainTest {
	public static void main(String[] args) {
		Command command = new ConcreteCommand(new Receiver());
		Invoker invoker = new Invoker(command);
		invoker.executeCommand();
	}
}
