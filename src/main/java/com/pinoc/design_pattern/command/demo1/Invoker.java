package com.pinoc.design_pattern.command.demo1;

/**
 * @author pinoc
 * @date 2023/10/22
 */
public class Invoker {
	private Command command;

	public Invoker(Command command) {
		this.command = command;
	}

	public void executeCommand() {
		command.execute();
	}
}
