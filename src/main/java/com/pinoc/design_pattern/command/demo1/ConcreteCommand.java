package com.pinoc.design_pattern.command.demo1;

/**
 * @author pinoc
 * @date 2023/10/22
 */
public class ConcreteCommand implements Command{

	private Receiver receiver;

	public ConcreteCommand(Receiver receiver) {
		this.receiver = receiver;
	}

	@Override
	public void execute() {
		receiver.action();
	}
}
