package com.pinoc.design_pattern.command.demo2;

/**
 * @author pinoc
 * @date 2023/12/8
 * 实现编辑器功能
 */
public class MainTest {
	public static void main(String[] args) {
		StringBuilder textEditor = new StringBuilder();
		CommandInvoker invoker = new CommandInvoker();

		invoker.execute(new TextInsertCommand("hello, ", textEditor, 0));
		invoker.execute(new TextInsertCommand("world!", textEditor, 7));

		invoker.execute(new TextDeleteCommand(textEditor, 5, 2));

		System.out.println("current text: " + textEditor.toString());

		invoker.undo();
		System.out.println("current text: " + textEditor.toString());

		invoker.undo();
		System.out.println("current text: " + textEditor.toString());

		invoker.redo();
		System.out.println("current text: " + textEditor.toString());
	}
}
