package com.pinoc.design_pattern.command.demo2;

/**
 * @author pinoc
 * @date 2023/12/8
 */
public class TextInsertCommand implements Command{

	private String text;
	private StringBuilder textEditor;
	private int position;

	public TextInsertCommand(String text, StringBuilder textEditor, int position) {
		this.text = text;
		this.textEditor = textEditor;
		this.position = position;
	}

	@Override
	public void execute() {
		textEditor.insert(position, text);
	}

	@Override
	public void undo() {
		textEditor.delete(position,position + text.length());
	}
}
