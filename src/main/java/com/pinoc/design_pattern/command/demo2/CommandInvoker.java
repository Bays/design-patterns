package com.pinoc.design_pattern.command.demo2;

import java.util.Stack;

/**
 * @author pinoc
 * @date 2023/12/8
 * 命令控制器
 */
public class CommandInvoker {
	Stack<Command> commandStack  = new Stack<>();
	Stack<Command> undoStack  = new Stack<>();

	public void execute(Command command) {
		command.execute();;
		commandStack.push(command);
	}

	public void undo() {
		if (!commandStack.isEmpty()) {
			Command popCommand = commandStack.pop();
			popCommand.undo();
			undoStack.push(popCommand);
		}
	}

	public void redo() {
		if (!undoStack.isEmpty()) {
			Command popCommand = undoStack.pop();
			popCommand.execute();
			commandStack.push(popCommand);
		}
	}
}

