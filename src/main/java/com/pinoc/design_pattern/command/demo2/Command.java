package com.pinoc.design_pattern.command.demo2;

/**
 * @author pinoc
 * @date 2023/12/8
 * 编辑器
 */
public interface Command {
	void execute();
	void undo();
}
