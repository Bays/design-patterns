package com.pinoc.design_pattern.command.demo2;

/**
 * @author pinoc
 * @date 2023/12/8
 */
public class TextDeleteCommand implements Command{

	private String text;
	private StringBuilder textEditor;
	private int position;

	public TextDeleteCommand( StringBuilder textEditor, int position, int length) {
		this.textEditor = textEditor;
		this.position = position;
		this.text = textEditor.substring(position, position + length);
	}

	@Override
	public void execute() {
		textEditor.delete(position,position + text.length());
	}

	@Override
	public void undo() {
		textEditor.insert(position, text);
	}
}
