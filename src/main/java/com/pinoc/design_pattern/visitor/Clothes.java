package com.pinoc.design_pattern.visitor;

/**
 * @author pinoc
 * @date 2023/10/24
 */
public class Clothes implements Item {
	private double price;

	public Clothes(double price) {
		this.price = price;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
