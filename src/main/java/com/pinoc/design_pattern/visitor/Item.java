package com.pinoc.design_pattern.visitor;

/**
 * @author pinoc
 */
public interface Item {

	void accept(Visitor visitor);
}
