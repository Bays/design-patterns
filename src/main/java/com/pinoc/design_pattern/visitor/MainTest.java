package com.pinoc.design_pattern.visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author pinoc
 * @date 2023/10/24
 */
public class MainTest {
	public static void main(String[] args) {
		List<Item> list = new ArrayList<>();
		list.add(new Book(50));
		list.add(new Book(30));
		list.add(new Clothes(100));
		list.add(new Clothes(200));

		RegularPriceVisitor regularPriceVisitor = new RegularPriceVisitor();
		DiscountPriceVisitor discountPriceVisitor = new DiscountPriceVisitor();
		for (Item item : list) {
			item.accept(regularPriceVisitor);
			item.accept(discountPriceVisitor);
		}
		System.out.println("regular totalCost: " + regularPriceVisitor.getTotalCost());
		System.out.println("discount totalCost: " + discountPriceVisitor.getTotalCost());
	}
}
