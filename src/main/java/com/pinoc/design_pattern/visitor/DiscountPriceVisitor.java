package com.pinoc.design_pattern.visitor;

/**
 * @author pinoc
 * @date 2023/10/24
 */
public class DiscountPriceVisitor implements Visitor{
	private double totalCost = 0;

	@Override
	public void visit(Book book) {
		totalCost += book.getPrice() * 0.8;
	}

	@Override
	public void visit(Clothes clothes) {
		totalCost += clothes.getPrice() * 0.9;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
}
