package com.pinoc.design_pattern.visitor;

/**
 * @author pinoc
 */
public interface Visitor {

	void visit(Book book);

	void visit(Clothes clothes);
}
