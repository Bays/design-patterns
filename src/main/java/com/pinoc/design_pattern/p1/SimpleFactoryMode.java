package com.pinoc.design_pattern.p1;

/**
 * 简单工厂模式 Test
 *
 * @author yinpeng10
 */
public class SimpleFactoryMode {

	public static void main(String[] args) {
		try{
			Car benz = Factory.driveCar("bmw");
			benz.drive();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
