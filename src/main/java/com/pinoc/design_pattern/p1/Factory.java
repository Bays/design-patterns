package com.pinoc.design_pattern.p1;

/**
 * 工厂类角色
 * @author yinpeng10
 */
public class Factory {

	public static Car driveCar(String car) throws Exception {
		if(car.equals("Benz")) {
			return new Benz();
		} else if(car.equals("bmw")) {
			return new Bmw();
		} else {
			throw new Exception();
		}
	}
}
