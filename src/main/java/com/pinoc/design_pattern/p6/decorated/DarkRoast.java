package com.pinoc.design_pattern.p6.decorated;

/**
 * @author yinpeng
 */
public class DarkRoast extends Beverage {

	public DarkRoast() {
		desc = "darkRoast";
	}

	@Override
	public double cost() {
		return 3.9;
	}
}
