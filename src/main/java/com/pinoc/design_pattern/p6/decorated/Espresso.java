package com.pinoc.design_pattern.p6.decorated;

/**
 * @author yinpeng
 * 浓缩咖啡
 */
public class Espresso extends Beverage {

	public Espresso() {
		desc = "Espresso";
	}

	@Override
	public double cost() {
		return 1.99; //饮料价格
	}
}
