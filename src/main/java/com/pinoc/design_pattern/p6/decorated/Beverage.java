package com.pinoc.design_pattern.p6.decorated;

/**
 * @author yinpeng
 * 被装饰者超类
 */
public abstract class Beverage {
	String desc = "unknown beverage";

	public String getDesc() {
		return desc;
	}

	public abstract double cost();
}
