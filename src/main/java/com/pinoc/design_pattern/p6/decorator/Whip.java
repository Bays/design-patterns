package com.pinoc.design_pattern.p6.decorator;

import com.pinoc.design_pattern.p6.decorated.Beverage;

/**
 * @author yinpeng
 */
public class Whip extends CondimentDecorator {

	Beverage beverage;

	public Whip(Beverage beverage) {
		this.beverage = beverage;
	}

	@Override
	public String getDesc() {
		return beverage.getDesc() +", whip";
	}

	@Override
	public double cost() {
		return 1.0 + beverage.cost();
	}
}
