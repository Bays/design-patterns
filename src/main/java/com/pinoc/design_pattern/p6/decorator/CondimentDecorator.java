package com.pinoc.design_pattern.p6.decorator;

import com.pinoc.design_pattern.p6.decorated.Beverage;

/**
 * @author yinpeng
 */
public abstract class CondimentDecorator extends Beverage {

	@Override
	public abstract String getDesc();

	@Override
	public double cost() {
		return 0;
	}
}
