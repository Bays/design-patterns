package com.pinoc.design_pattern.p6.decorator;

import com.pinoc.design_pattern.p6.decorated.Beverage;

/**
 * @author yinpeng
 */
public class Mocha extends CondimentDecorator {

	Beverage beverage;

	public Mocha(Beverage beverage) {
		this.beverage = beverage;
	}

	@Override
	public String getDesc() {
		return beverage.getDesc() + ", mocha";
	}

	@Override
	public double cost() {
		return .20 + beverage.cost();
	}
}
