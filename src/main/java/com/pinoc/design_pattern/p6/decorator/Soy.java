package com.pinoc.design_pattern.p6.decorator;

import com.pinoc.design_pattern.p6.decorated.Beverage;

/**
 * @author yinpeng
 */
public class Soy extends CondimentDecorator {

	Beverage beverage;

	public Soy(Beverage beverage) {
		this.beverage = beverage;
	}

	@Override
	public double cost() {
		return .50 + beverage.cost();
	}

	@Override
	public String getDesc() {
		return beverage.getDesc() +" , soy";
	}
}
