package com.pinoc.design_pattern.p6;

		import com.pinoc.design_pattern.p6.decorated.Beverage;
		import com.pinoc.design_pattern.p6.decorated.DarkRoast;
		import com.pinoc.design_pattern.p6.decorated.Espresso;
		import com.pinoc.design_pattern.p6.decorator.Mocha;
		import com.pinoc.design_pattern.p6.decorator.Whip;

/**
 * @author yinpeng
 */
public class TestDecorator {

	public static void main(String[] args) {
		Beverage beverage = new Espresso();
		System.out.println(beverage.getDesc() +" $"+beverage.cost());

		Beverage beverage1 = new DarkRoast(); //3.9
		beverage1 = new Mocha(beverage1);  //0.2
		beverage1 = new Mocha(beverage1);  //0.2
		beverage1 = new Whip(beverage1); //1.0
		System.out.println(beverage1.getDesc() + " $" + beverage1.cost());
		//3.9 + 0.2 *2 + 1 = 5.3
	}
}
