package com.pinoc.design_pattern.memento;

import java.util.Stack;

/**
 * @author pinoc
 * @date 2023/12/5
 */
public class CalculatorHistory {
	private final Stack<CalculatorMemento> history = new Stack<>();

	public void save(Calculator calculator) {
		history.push(calculator.save());
	}

	public void undo(Calculator calculator) {
		if (!history.isEmpty()) {
			calculator.restore(history.pop());
		}
	}
}
