package com.pinoc.design_pattern.memento;

/**
 * @author pinoc
 * @date 2023/12/5
 */
public class CalculatorMain {
	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		CalculatorHistory calculatorHistory = new CalculatorHistory();

		calculator.add(5);
		calculator.subtract(2);
		calculatorHistory.save(calculator);
		System.out.println("Result: " + calculator.getResult());

		calculator.add(10);
		calculatorHistory.save(calculator);
		System.out.println("Result: " + calculator.getResult());

		calculator.add(10);
		System.out.println("Result: " + calculator.getResult());

		calculatorHistory.undo(calculator);
		System.out.println("Result: " + calculator.getResult());

		calculatorHistory.undo(calculator);
		System.out.println("Result: " + calculator.getResult());
	}
}
