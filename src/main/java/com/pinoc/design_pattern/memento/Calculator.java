package com.pinoc.design_pattern.memento;

/**
 * @author pinoc
 * @date 2023/12/5
 */
public class Calculator {
	private int result;

	public void add(int value) {
		result += value;
	}

	public void subtract(int value) {
		result -= value;
	}

	public CalculatorMemento save() {
		return new CalculatorMemento(result);
	}

	public void restore(CalculatorMemento calculatorMemento) {
		result = calculatorMemento.getResult();
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
}
