package com.pinoc.design_pattern.memento;

/**
 * @author pinoc
 * @date 2023/12/5
 */
public class CalculatorMemento {
	private final int result;

	public CalculatorMemento(int result) {
		this.result = result;
	}

	public int getResult() {
		return result;
	}
}
