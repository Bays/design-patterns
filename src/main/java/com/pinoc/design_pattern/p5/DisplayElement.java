package com.pinoc.design_pattern.p5;

/**
 * @author yinpeng
 */
public interface DisplayElement {

	void display();
}
