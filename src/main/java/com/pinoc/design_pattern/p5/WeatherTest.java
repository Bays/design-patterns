package com.pinoc.design_pattern.p5;

import com.pinoc.design_pattern.p5.observer.CurrentConditionsDisplayObserver;
import com.pinoc.design_pattern.p5.observer.ForecastDisplayObserver;
import com.pinoc.design_pattern.p5.subject.WeatherDataSubject;

/**
 * @author yinpeng
 */
public class WeatherTest {

	public static void main(String[] args) {
		WeatherDataSubject weatherDataSubject = new WeatherDataSubject();
		CurrentConditionsDisplayObserver currentConditionsDisplayObserver = new CurrentConditionsDisplayObserver(weatherDataSubject);
		ForecastDisplayObserver forecastDisplayObserver = new ForecastDisplayObserver(weatherDataSubject);
		weatherDataSubject.setMeasurements(80,40,40.4f);
		weatherDataSubject.measurementsChanged();
	}
}
