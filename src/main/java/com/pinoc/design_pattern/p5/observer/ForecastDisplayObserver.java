package com.pinoc.design_pattern.p5.observer;

import com.pinoc.design_pattern.p5.DisplayElement;
import com.pinoc.design_pattern.p5.subject.WeatherDataSubject;

import java.util.Observable;
import java.util.Observer;

/**
 * @author yinpeng
 */
public class ForecastDisplayObserver implements Observer, DisplayElement {

	Observable observable;
	private float currentPressure = 29.92f;
	private float lastPressure;

	public ForecastDisplayObserver(Observable observable) {
		this.observable = observable;
		observable.addObserver(this);
	}

	@Override
	public void display() {
		System.out.println("last pressure: " + lastPressure + " current pressure: " + currentPressure);
	}

	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof WeatherDataSubject) {
			WeatherDataSubject weatherDataSubject = (WeatherDataSubject) o;
			this.lastPressure = currentPressure;
			this.currentPressure = weatherDataSubject.getPressure();
			display();
		}
	}

}
