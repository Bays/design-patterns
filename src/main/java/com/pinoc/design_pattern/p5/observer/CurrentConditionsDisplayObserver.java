package com.pinoc.design_pattern.p5.observer;

import com.pinoc.design_pattern.p5.DisplayElement;
import com.pinoc.design_pattern.p5.subject.WeatherDataSubject;

import java.util.Observable;
import java.util.Observer;

/**
 * @author yinpeng
 * java.util.Observer 内置方法
 */
public class CurrentConditionsDisplayObserver implements Observer, DisplayElement {

	Observable observable;

	private float temperature;

	private float humidity;

	public CurrentConditionsDisplayObserver(Observable observable) {
		this.observable = observable;
		observable.addObserver(this);
	}

	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof WeatherDataSubject) {
			WeatherDataSubject weatherDataSubject = (WeatherDataSubject) o;
			this.temperature = weatherDataSubject.getTemperature();
			this.humidity = weatherDataSubject.getHumidity();
			display();
		}
	}

	@Override
	public void display() {
		System.out.println("current conditions: " + temperature + " F degrees and " + humidity + "% humidity");
	}
}
