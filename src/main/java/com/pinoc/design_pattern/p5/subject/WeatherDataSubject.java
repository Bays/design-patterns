package com.pinoc.design_pattern.p5.subject;

import java.util.Observable;

/**
 * @author yinpeng
 * java.util.Observable 内置方法
 */
public class WeatherDataSubject extends Observable {

	private float temperature;

	private float humidity;

	private float pressure;

	public WeatherDataSubject() {
	}

	public void setMeasurements(float temperature, float humidity, float pressure){
		this.temperature = temperature;
		this.humidity = humidity;
		this.pressure = pressure;

	}
	public void measurementsChanged() {
		setChanged(); //java.util.Observable 内置方法
		notifyObservers(); //java.util.Observable 内置方法
	}

	public float getTemperature() {
		return temperature;
	}

	public float getHumidity() {
		return humidity;
	}

	public float getPressure() {
		return pressure;
	}
}
