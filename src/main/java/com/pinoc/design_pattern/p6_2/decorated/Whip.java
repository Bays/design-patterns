package com.pinoc.design_pattern.p6_2.decorated;

import com.pinoc.design_pattern.p6_2.decorator.Beverage;
import com.pinoc.design_pattern.p6_2.decorator.CondimentDecorator;

/**
 * @author yinpeng
 */
public class Whip extends CondimentDecorator {

	Beverage beverage;

	public Whip(Beverage beverage) {
		this.beverage = beverage;
	}

	@Override
	public String getDesc() {
		return beverage.getDesc() + ", whip";
	}

	@Override
	public double cost() {
		return 0.5 + beverage.cost();
	}
}
