package com.pinoc.design_pattern.p6_2.decorated;

import com.pinoc.design_pattern.p6_2.decorator.Beverage;
import com.pinoc.design_pattern.p6_2.decorator.CondimentDecorator;

/**
 * @author yinpeng
 */
public class Mocha extends CondimentDecorator {

	Beverage beverage;

	public Mocha(Beverage beverage) {
		this.beverage = beverage;
	}

	@Override
	public double cost() {
		return 0.20 + beverage.cost();
	}

	@Override
	public String getDesc() {
		return beverage.getDesc() + "" + ", mocha";
	}
 }
