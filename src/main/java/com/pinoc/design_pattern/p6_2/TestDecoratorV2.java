package com.pinoc.design_pattern.p6_2;

import com.pinoc.design_pattern.p6_2.decorated.Mocha;
import com.pinoc.design_pattern.p6_2.decorated.Whip;
import com.pinoc.design_pattern.p6_2.decorator.Beverage;
import com.pinoc.design_pattern.p6_2.decorator.Espresso;
import com.pinoc.design_pattern.p6_2.decorator.HouseBlend;

/**
 * @author yinpeng
 */
public class TestDecoratorV2 {
	public static void main(String[] args) {

		Beverage beverageNothing = new Espresso();
		System.out.println(beverageNothing.getDesc() +" $" + beverageNothing.cost());

		Beverage espresson = new Espresso();
		espresson = new Mocha(espresson);
		espresson = new Mocha(espresson);
		System.out.println(espresson.getDesc() +" $"+espresson.cost());

		Beverage houseBlend = new HouseBlend();
		houseBlend = new Mocha(houseBlend);
		houseBlend = new Whip(houseBlend);
		System.out.println(houseBlend.getDesc() +" $"+houseBlend.cost());
	}
}
