package com.pinoc.design_pattern.p6_2.decorator;

/**
 * @author yinpeng
 */
public class HouseBlend extends Beverage {

	public HouseBlend() {
		desc = "HouseBlend";
	}

	@Override
	public double cost() {
		return 2.55;
	}
}
