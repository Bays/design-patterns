package com.pinoc.design_pattern.p6_2.decorator;

/**
 * @author yinpeng
 */
public abstract class CondimentDecorator extends Beverage {

	@Override
	public abstract String getDesc();

}
