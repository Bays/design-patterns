package com.pinoc.design_pattern.p6_2.decorator;

/**
 * @author yinpeng
 */
public abstract class Beverage {

	String desc = "unknown beverage";

	public String getDesc() {
		return desc;
	}

	public abstract double cost();
}
