package com.pinoc.design_pattern.p6_2.decorator;


/**
 * @author yinpeng
 */
public class Espresso extends Beverage {

	public Espresso() {
		desc = "Espresso";
	}

	@Override
	public double cost() {
		return 1.99;
	}
}
