package com.pinoc.callback;

/**
 * @author pinoc
 * @date 2024/5/8
 */

public class Boss implements CallBack<String>{
	private Worker worker;

	public Boss() {
	}

	public Boss(Worker worker) {
		this.worker = worker;
	}

	@Override
	public void callback(String result) {
		System.out.println("通知老板: " + result);
	}

	/**
	 * 安排工作
	 * @param someDetail
	 */
	public void bossTodo(final String someDetail) {
		System.out.println("老板安排工作");
		new Thread(() -> worker.work(Boss.this, someDetail)).start();
		System.out.println("老板安排工作完成");
		System.out.println("老板下班");
	}

	public Worker getWorker() {
		return worker;
	}

	public void setWorker(Worker worker) {
		this.worker = worker;
	}
}
