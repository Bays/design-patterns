package com.pinoc.callback;

/**
 * @author pinoc
 * @date 2024/5/8
 */
public class Worker {
	public void work(CallBack<String> boss, String someWork) {
		String result = someWork + " 完成";
		boss.callback(result);
	}

	public static void main(String[] args) {
		Worker worker = new Worker();
		Boss boss = new Boss(worker);
		boss.bossTodo("干活！！！");
	}
}
