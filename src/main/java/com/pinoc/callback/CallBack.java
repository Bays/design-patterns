package com.pinoc.callback;

/**
 * @author pinoc
 */
public interface CallBack<T> {
	public void callback(T t);
}
